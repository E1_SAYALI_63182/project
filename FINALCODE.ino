/*Agricultural monitoring system with Thingspeak

 */
 
#include <ESP8266WiFi.h>
#include "DHT.h"

String apiKey = "6B7TDTND588YXXM9";
const char *ssid =  "vivo 1920";
const char *pass =  "pragati77";
const char* server = "api.thingspeak.com";

DHT dht(D2, DHT11);
int pir_sensor =  D1;

WiFiClient client;

void setup() {
  Serial.begin(115200);
  delay(10);
  pinMode(pir_sensor,INPUT);
  dht.begin();
  WiFi.begin(ssid, pass);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  pinMode(A0,INPUT);
}

void loop() {
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  int moisture=analogRead(A0);
  int pir = digitalRead(pir_sensor);

  if (isnan(h) || isnan(t)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }

  if (client.connect(server, 80)) {
    String postStr = apiKey;
    postStr += "&field1=";
    postStr += String(t);
    postStr += "&field2=";
    postStr += String(h);
    postStr += "&field3=";
    postStr += String(moisture);
    postStr += "&field4=";
    postStr += String(pir);
    postStr += "\r\n\r\n";

    client.print("POST /update HTTP/1.1\n");
    client.print("Host: api.thingspeak.com\n");
    client.print("Connection: close\n");
    client.print("X-THINGSPEAKAPIKEY: " + apiKey + "\n");
    client.print("Content-Type: application/x-www-form-urlencoded\n");
    client.print("Content-Length: ");
    client.print(postStr.length());
    client.print("\n\n");
    client.print(postStr);

    Serial.print("Temperature: ");
    Serial.print(t);
    Serial.print("\t");
    Serial.print("Humidity: ");
    Serial.println(h);
    Serial.print("Moisture content in soil in Percentage:");
    Serial.println((moisture/1024)*100);
    Serial.println(moisture);
    Serial.print("motion : ");
    Serial.println(pir);

  }
  client.stop();
  delay(1000);
}
